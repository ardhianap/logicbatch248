﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02
{
    class Soal10
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n :");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan nilai n2 :");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];

            //Value creation
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else
                    {
                        //jika j = 0 maka nol
                        if (j < 1)
                        {
                            array2D[i, j] = 0;
                        }
                        else if (j == 1)
                        {
                            //jika baris =  1  dan i = 1 maka nilai sesuai input n2
                            if (i == 1)
                            {
                                array2D[i, j] = n2;
                            }
                            //jika baris = 2 dan j bukan 1 maka nilai input n2 + j
                            else
                            {
                                array2D[i, j] = n2 + j;
                            }   
                        }
                        else
                        {
                            //jika baris = 1 dan i = 1 maka nilai j sebelumnya tambah nilai n2
                            if (i == 1)
                            {
                                array2D[i, j] = array2D[i, j - 1] + n2;
                            }
                            //jika baris = 2 dan i bukan 1 maka nilai i sebelumnya tambah j
                            else
                            {
                                array2D[i, j] = array2D[i - 1, j] + j;
                            }
                        }
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
