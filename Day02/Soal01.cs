﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02
{
    class Soal01
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n :");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan nilai n2 :");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[2, n];
            //int angkaAwal = 1;

            //Value creation
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if ( i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else
                    {
                        array2D[i, j] = Convert.ToInt32(Math.Pow(n2, j));
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
