﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02
{
    class Soal06
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n :");
            int n = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];

            //Value creation
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        array2D[i, j] = Convert.ToInt32(Math.Pow(n, j));
                    }
                    else
                    {
                        array2D[i, j] = Convert.ToInt32(Math.Pow(n, j)) + j;
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
