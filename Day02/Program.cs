﻿using Day02;
using System;


namespace Day01
{
    class Program
    {
        static void Main(string[] args)
        {

            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal :");
                int nomorSoal = int.Parse(Console.ReadLine());
            
                switch (nomorSoal)
                {
                    case 1:
                        Soal01.Resolve();
                        break;

                    case 2:
                        Soal02.Resolve();
                        break;

                    case 3:
                        Soal03.Resolve();
                        break;
                    case 4:
                        Soal04.Resolve();
                        break;
                    case 5:
                        Soal05.Resolve();
                        break;
                    case 6:
                        Soal06.Resolve();
                        break;
                    case 7:
                        Soal07.Resolve();
                        break;
                    case 8:
                        Soal08.Resolve();
                        break;
                    case 9:
                        Soal09.Resolve();
                        break;
                    case 10:
                        Soal10.Resolve();
                        break;


                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;  
                }

                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
     
            }
            
        }
    }
}




