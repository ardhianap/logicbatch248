﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02
{
    class Soal09
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n :");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan nilai n2 :");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];

            //Value creation
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        //jika i = 1 dan j = 0
                        if (j == 0)
                        {
                            array2D[i, j] = 0;
                        }
                        //jika i = 1 dan j bukan 0 maka tambah berdasar nilai input n2
                        else
                        {
                            array2D[i, j] = array2D[i, j-1] + n2;
                        }
                    }
                    else
                    {
                        //jika i = 2 dan j = 0 ambil dari nilai terakhir dari sebelumnya
                        if (j == 0)
                        {
                            array2D[i, j] = array2D[i - 1, j + (n - 1)];
                        }
                        //jika i = 2 dan j bukan 0 maka kurang berdasar n2
                        else
                        {
                            array2D[i, j] = array2D[i, j - 1] - n2;
                        }
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
