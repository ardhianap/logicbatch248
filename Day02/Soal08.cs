﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02
{
    class Soal08
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n :");
            int n = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];

            //Value creation
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else 
                    {
                        //jika j = 0
                        if (j < 1)
                        {
                            array2D[i, j] = 0;
                        }
                        //jika j bukan 0
                        else 
                        {
                           array2D[i, j] = array2D[i-1,j] + j;   
                        }
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
