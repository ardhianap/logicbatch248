﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Day02
{
    class Soal02
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n :");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan nilai n2 :");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[2, n];
            int angkaKelipatan = 0;

            //Value creation
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        angkaKelipatan++;
                        if (angkaKelipatan % 3 == 0)
                        {
                            array2D[i, j] = Convert.ToInt32(Math.Pow(n2, j));
                            array2D[i, j] *= -1;
                        }
                        else
                        {
                            array2D[i, j] = Convert.ToInt32(Math.Pow(n2, j));
                        }
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }   
    }
}
