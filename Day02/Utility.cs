﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02
{
    class Utility
    {
        public static void PrintArray2D(int[,] array2D)
        {
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    Console.Write(array2D[i, j] + " " + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
