﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {

            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal :");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Solve me first");
                        SolveMeFirst.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("02. Time conversion");
                        TimeConversion.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("03. Simple array sum");
                        SimpleArraySum.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("04. Diagonal difference");
                        DiagonalDifference.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("05. Plus minus");
                        PlusMinus.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("06. Staircase");
                        Staircase.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("07. Mini max sum");
                        MiniMaxSum.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("08. Birthday cake candles");
                        BirthdayCakeCandles.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("09. A very big sum");
                        AVeryBigSum.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("10. Compare the triplets");
                        CompareTheTriplets.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();

            }

        }
    }
}
