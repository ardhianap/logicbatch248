﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Staircase
    {
        public static void Resolve()
        {
            Console.WriteLine();
            //input nilai
            Console.WriteLine("Masukkan panjang bilangan matrix");
            int length = int.Parse(Console.ReadLine());

            int[,] array2D = new int[length, length];

            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i + j < (array2D.GetLength(1) - 1))
                    {
                        Console.Write("" + " " + "\t");
                    }
                    else
                    {
                        Console.Write("#" + " " + "\t");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
