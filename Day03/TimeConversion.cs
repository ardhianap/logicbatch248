﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConversion
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan inputan waktu dalam format 12 jam");
            String time = Console.ReadLine();

            if (!time.Contains("AM") && !time.Contains("PM"))
            {
                Console.WriteLine("Format waktu yang anda masukkan SALAH!");
            }
            else
            {
                try
                {
                    DateTime dateTime = Convert.ToDateTime(time);
                    Console.Write("Format Waktu Dalam 24 Jam adalah ");
                    Console.WriteLine(dateTime.ToString("HH:mm:ss"));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Format waktu yang anda masukkan SALAH!");
                }

            }
        }
    }
}


