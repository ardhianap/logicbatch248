﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            Console.WriteLine();
            //input nilai
            Console.WriteLine("Masukkan panjang bilangan matrix");
            int length = int.Parse(Console.ReadLine());
            int[,] array2D = new int[length, length];

            try
            {
                for (int i = 0; i < length; i++)
                {
                    Console.WriteLine("Masukkan Himpunan Angka Baris ke-" + (i + 1));
                    string numbers = Console.ReadLine();

                    int[] array = Utility.ConvertStringToIntArray(numbers);
                    if (array.Length != length)
                    {
                        Console.WriteLine("Angka Salah, Input Ulang !");
                        i = -1;
                    }
                    else
                    {
                        for (int j = 0; j < length; j++)
                        {
                            array2D[i, j] = array[j];
                        }
                    }
                }
                Console.WriteLine();

                int diagonalKanan = 0;
                int diagonalKiri = 0;

                //proses inputan
                //diagonal atas kiri kekanan
                for (int i = 0; i < array2D.GetLength(0); i++)
                {
                    for (int j = 0; j < array2D.GetLength(1); j++)
                    {
                        if (i == j)
                        {
                            diagonalKanan = diagonalKanan + array2D[i, j];
                        }
                    }
                }

                //diagonal atas kanan kekiri
                for (int i = 0; i < array2D.GetLength(0); i++)
                {
                    for (int j = 0; j < array2D.GetLength(1); j++)
                    {
                        if (i + j == (array2D.GetLength(1) - 1))
                        {
                            diagonalKiri = diagonalKiri + array2D[i, j];
                        }
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Penjumlahan diagonal kanan atas ke bawah kiri : " + diagonalKanan);
                Console.WriteLine("Penjumlahan diagonal kiri atas ke bawah kanan : " + diagonalKiri);
                Console.WriteLine("Nilai absolut |x| = " + Math.Abs(diagonalKanan - diagonalKiri));
            }
            catch (Exception e)
            {
                Console.WriteLine("Format Anda Salah");
            }
            
        }
    }
}
