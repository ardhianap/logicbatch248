﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class BirthdayCakeCandles
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan himpunan bilangan: ");
            String numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int maximal = numbersArray.Max();
            int jumlahMax = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] == maximal)
                {
                    jumlahMax++;
                }
            }

            Console.WriteLine("Jumlah Lilin Tertinggi Adalah " + jumlahMax);
        }
    }
}
