﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class MiniMaxSum
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan himpunan bilangan: ");
            String numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int total = 0;
            int minimal = numbersArray.Min();
            int maximal = numbersArray.Max();
            int totalMin = 0;
            int totalMax = 0;


            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }

            totalMin = total - minimal;
            totalMax = total - maximal;

            Console.WriteLine("Nilai Penjumlahan Minimal Adalah " + totalMin);
            Console.WriteLine("Nilai Penjumlahan Maximal Adalah " + totalMax);
        }
    }
}
