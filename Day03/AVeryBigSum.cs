﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class AVeryBigSum
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan himpunan bilangan: ");
            String numbers = Console.ReadLine();

            long[] numbersArray = Utility.ConvertStringToLongArray(numbers);
            long total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }

            Console.WriteLine("Nilai Total Adalah " + total);
        }
    }
}
