﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan himpunan bilangan: ");
            String numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            float total = 0.0f;
            float bilPositif = 0.0f;
            float bilNol = 0.0f;
            float bilNegatif = 0.0f;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total = numbersArray.Length;
                
                if (numbersArray[i] < 0)
                {
                    bilNegatif++;
                }
                else if (numbersArray[i] > 0)
                {
                    bilPositif++;
                }
                else
                {
                    bilNol++;
                }
            }

            float totalPositif = bilPositif / total;
            float totalNegatif = bilNegatif / total;
            float totalNol = bilNol / total;

            Console.WriteLine("Jumlah bilangan yang di input adalah "+total);
            Console.WriteLine("Nilai persen dari bilangan positif adalah "+ totalPositif);
            Console.WriteLine("Nilai persen dari bilangan nol adalah "+ totalNegatif);
            Console.WriteLine("Nilai persen dari bilangan negatif adalah "+ totalNol);
        }
    }
}
