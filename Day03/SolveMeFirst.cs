﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class SolveMeFirst
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan angka pertama :");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan angka kedua :");
            int number2 = int.Parse(Console.ReadLine());

            int total = Utility.Sum(number1, number2);

            Console.WriteLine("Nilai Total adalah : " + total);
        }
    }
}
