﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class CompareTheTriplets
    {
        public static void Resolve()
        {
            Console.WriteLine();
            //input nilai
            Console.WriteLine("Masukkan panjang bilangan baris 1 : ");
            String bilanganBaris1 = Console.ReadLine();

            Console.WriteLine("Masukkan panjang bilangan baris 2 : ");
            String bilanganBaris2 = Console.ReadLine();

            int[] arrayBaris = Utility.ConvertStringToIntArray(bilanganBaris1);
            int[] arrayBaris2 = Utility.ConvertStringToIntArray(bilanganBaris2);

            int nilaiBaris1 = 0;
            int nilaiBaris2 = 0;

            if (arrayBaris.Length != arrayBaris2.Length)
            {
                Console.WriteLine("Jumlah bilangan yang anda masukkan SALAH!");
            }
            else
            {
                for (int i = 0; i < arrayBaris.Length; i++)
                {
                    if (arrayBaris[i] < arrayBaris2[i])
                    {
                        nilaiBaris2++;
                    }
                    else if (arrayBaris[i] > arrayBaris2[i])
                    {
                        nilaiBaris1++;
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Score Pertama : " + nilaiBaris1);
                Console.WriteLine("Score Kedua : " + nilaiBaris2);
            }       
        }
    }
}
