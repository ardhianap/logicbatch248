﻿using System;


namespace Day01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Masukkan nomor soal :");
            int nomorSoal = int.Parse(Console.ReadLine());

            if (nomorSoal == 1)
            {

                //soal perhitungan BMI

                Console.WriteLine("Masukkan tinggi dalam satuan meter : ");
                double tinggi = double.Parse(Console.ReadLine());

                Console.WriteLine("Masukkan berat dalam satuan kilogram : ");
                double berat = double.Parse(Console.ReadLine());

                double BMI = berat / (tinggi * tinggi);

                if (BMI < 18.5)
                {
                    Console.WriteLine("BMI : " + BMI);
                    Console.WriteLine("BMI termasuk Underweight");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("BMI : " + BMI);
                    Console.WriteLine("BMI termasuk Overweight");
                }
                else
                {
                    Console.WriteLine("BMI : " + BMI);
                    Console.WriteLine("BMI termasuk Normal");
                }

            }

            else if (nomorSoal == 2)
            {
                //soal deret 1 dimensi LMS

                Console.WriteLine("Masukkan panjang angka: ");
                int length = int.Parse(Console.ReadLine());

                //soal 01
                int[] angkaGanjilArray = new int[length];
                int angkaGanjil = 1;

                //masukkan dan cetak value
                for (int i = 0; i < length; i++)
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil += 2;
                    Console.Write(angkaGanjilArray[i] + " ");
                }

                Console.WriteLine();

                //soal 02
                int[] angkaGenapArray = new int[length];
                int angkaGenap = 2;

                //masukkan dan cetak value
                for (int i = 0; i < length; i++)
                {
                    angkaGenapArray[i] = angkaGenap;
                    angkaGenap += 2;
                    Console.Write(angkaGenapArray[i] + " ");
                }
                Console.WriteLine();

                //soal 03
                int[] soalArray3 = new int[length];
                int angkaAwalSoal3 = 1;

                for (int i = 0; i < length; i++)
                {
                    soalArray3[i] = angkaAwalSoal3;
                    angkaAwalSoal3 = angkaAwalSoal3 + 3;
                    Console.Write(soalArray3[i] + " ");
                }
                Console.WriteLine();

                //soal 04
                int[] soalArray4 = new int[length];
                int angkaAwalSoal4 = 1;

                for (int i = 0; i < length; i++)
                {
                    soalArray4[i] = angkaAwalSoal4;
                    angkaAwalSoal4 = angkaAwalSoal4 + 4;
                    Console.Write(soalArray4[i] + " ");
                }
                Console.WriteLine();

                //soal 05
                int angkaAwalSoal5 = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("*" + " ");
                    }
                    else
                    {
                        Console.Write(angkaAwalSoal5 + " ");
                        angkaAwalSoal5 = angkaAwalSoal5 + 4;
                    }
                }
                Console.WriteLine();

                //soal 06
                int angkaAwalSoal6 = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("*" + " ");
                    }
                    else
                    {
                        Console.Write(angkaAwalSoal6 + " ");
                    }
                    angkaAwalSoal6 = angkaAwalSoal6 + 4;
                }
                Console.WriteLine();

                //soal 07
                int[] soalArray7 = new int[length];
                int angkaAwalSoal7 = 2;

                for (int i = 0; i < length; i++)
                {
                    soalArray7[i] = angkaAwalSoal7;
                    angkaAwalSoal7 = angkaAwalSoal7 * 2;
                    Console.Write(soalArray7[i] + " ");
                }
                Console.WriteLine();

                //soal 08
                int[] soalArray8 = new int[length];
                int angkaAwalSoal8 = 3;

                for (int i = 0; i < length; i++)
                {
                    soalArray8[i] = angkaAwalSoal8;
                    angkaAwalSoal8 = angkaAwalSoal8 * 3;
                    Console.Write(soalArray8[i] + " ");
                }
                Console.WriteLine();

                //soal 09
                int angkaAwalSoal9 = 4;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("*" + " ");
                    }
                    else
                    {
                        Console.Write(angkaAwalSoal9 + " ");
                        angkaAwalSoal9 = angkaAwalSoal9 * 4;
                    }
                }
                Console.WriteLine();

                //soal 10
                int angkaAwalSoal10 = 3;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 4 == 0)
                    {
                        Console.Write("XXX" + " ");
                    }
                    else
                    {
                        Console.Write(angkaAwalSoal10 + " ");
                    }
                    angkaAwalSoal10 = angkaAwalSoal10 * 3;
                }
                Console.WriteLine();

                //soal 11
                int[] fibonaciArray = new int[length];

                for (int i = 0; i < length; i++)
                {
                    if (i <= 1)
                    {
                        fibonaciArray[i] = 1;
                    }
                    else
                    {
                        fibonaciArray[i] = fibonaciArray[i-1] + fibonaciArray[i-2];
                    }
                    Console.Write(fibonaciArray[i] + " ");
                }  
                Console.WriteLine();

                //soal 12
                int[] doubleNilaiArray = new int[length];

                for (int i = 0; i < length; i++)
                {   
                    //panjang bilangan ganjil

                    if (length % 2 == 1)
                    {
                        if (i < 1)
                        {
                            doubleNilaiArray[i] = 1;
                        }
                        else if (i <= (length / 2))
                        {
                            doubleNilaiArray[i] = doubleNilaiArray[i - 1] + 2;
                        }
                        else
                        {
                            doubleNilaiArray[i] = doubleNilaiArray[i - 1] - 2;
                        }

                    }

                    //panjang bilangan genap

                    else
                    {
                        if (i < 1)
                        {
                            doubleNilaiArray[i] = 1;
                        }
                        else if (i < (length / 2))
                        {
                            doubleNilaiArray[i] = doubleNilaiArray[i - 1] + 2;
                        }
                        else if (i == (length / 2))
                        {
                            doubleNilaiArray[i] = doubleNilaiArray[i - 1];
                        }
                        else
                        {
                            doubleNilaiArray[i] = doubleNilaiArray[i - 1] - 2;
                        }
                    }
                    Console.Write(doubleNilaiArray[i] + " ");
                }
                Console.WriteLine();

                //soal13
                int[] fibonaciArray2 = new int[length];

                for (int i = 0; i < length; i++)
                {
                    if (i <= 2)
                    {
                        fibonaciArray2[i] = 1;
                    }
                    else
                    {
                        fibonaciArray2[i] = fibonaciArray2[i - 1] + fibonaciArray2[i - 2] + fibonaciArray2[i - 3];
                    }
                    Console.Write(fibonaciArray2[i] + " ");
                }
                Console.WriteLine();

                //soal 15
                Console.WriteLine("soal 15");
               
                int[] Fibonaci15 = new int[length];

                for (int i = 0; i < length; i++)
                {
                    //genap
                    if (length % 2 == 0)
                    {
                        if (i < length / 2)
                        {
                            Fibonaci15[i] = fibonaciArray[i];
                            Fibonaci15[length - 1 - i] = fibonaciArray[i];
                        }
                    }
                    else
                    //ganjil
                    {
                        if (i <= length / 2)
                        {
                            Fibonaci15[i] = fibonaciArray[i];
                            Fibonaci15[length - 1 - i] = fibonaciArray[i];
                        }
                    }
                    Console.Write(Fibonaci15[i] + " ");
                }
                Console.WriteLine();


                //soal14
                //int[] bilanganPrima = new int[length];
                //bool isPrima = false;

                //for (int i = 0; i < length; i++)
                //{
                //    if (bilanganPrima[i] / bilanganPrima[i])
                //    {
                //        isPrima = true;
                //    }
                //    else if
                //    {
                //        isPrima = true;
                //    }
                //    else
                //    {
                //        isPrima = false;
                //    }

                //    if (isPrima = true)
                //    {
                //        Console.Write(bilanganPrima[i] + " ");
                //    }
                //}
                Console.WriteLine();

            }
            else
            {
                Console.WriteLine("Nomor Soal Tidak Ditemukan");
            }

            Console.ReadKey();
        }
    }
}




