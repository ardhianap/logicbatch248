﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MarsExploration
    {
        public static void Resolve()
        {
            Console.WriteLine();

            //input kalimat
            Console.WriteLine("Masukan kalimat");
            string kalimat = Console.ReadLine();

            int hitungTotalS = 0;
            int hitungTotalO = 0;

            if (kalimat.Length % 3 != 0)
            {
                Console.WriteLine("Salah");
            }
            else
            {
                for (int i = 0; i < kalimat.Length; i++)
                {   //jika modulus 3 nilai i nya 0 dan 2 dan tidak mengandung S maka nilai hitung nya naik
                    if (i % 3 == 0 || i % 3 == 2)
                    {
                        if (kalimat[i] != 'S')
                        {
                            hitungTotalS++;
                        }
                    }

                    //jika modulus 3 nilai i nya 1 dan tidak mengandung 0 maka nilai hitungnya naik
                    if (i % 3 == 1)
                    {
                        if (kalimat[i] != 'O')
                        {
                            hitungTotalO++;
                        }
                    }
                }

                int hitungTotal = hitungTotalS + hitungTotalO;
                Console.WriteLine("Total : " + hitungTotal);
            }
            Console.WriteLine();
        }
    }
}
