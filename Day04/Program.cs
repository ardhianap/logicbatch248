﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {

            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal :");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Camel Case");
                        CamelCase.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("02. Strong Password");
                        StrongPassword.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("03. Caesar Cipher");
                        CaesarCipher.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("04. Mars Exploration");
                        MarsExploration.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("05. Hacker Rank In A String");
                        HackerRankInAString.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("06. Pangrams");
                        Pangrams.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("07. Separate The Numbers");
                        SeparateTheNumbers.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("08. Gemstones");
                        Gemstones.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("09. Making Anagrams");
                        MakingAnagrams.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("10. Two Strings");
                        TwoStrings.Resolve();
                        break;
                    case 11:
                        Console.WriteLine("11. Middle Asterisk 3");
                        MiddleAsterisk3.Resolve();
                        break;
                    case 12:
                        Console.WriteLine("12. Palindrome");
                        Palindrome.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();

            }

        }
    }
}
