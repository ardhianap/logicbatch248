﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CaesarCipher
    {
        public static void Resolve()
        {
            Console.WriteLine();

            int length, shift;

            Console.WriteLine("Masukkan kalimat yang diinput :");
            string input = Console.ReadLine();

            Console.WriteLine("Masukkan rotasi kalimat :");
            shift = Convert.ToInt32(Console.ReadLine());

            int[] alphabet = new int[input.Length];
            char[] hasil = new char[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                alphabet[i] = (int)input[i]; //ubah inputan ke integer
                if (alphabet[i] > 64 && alphabet[i] < 91)
                {
                    // % modulus 26 untuk shift diatas 26
                    alphabet[i] = (65 + ((alphabet[i] - 65) + shift) % 26);
                }
                else if (alphabet[i] > 96 && alphabet[i] < 123)
                {
                    alphabet[i] = (97 + ((alphabet[i] - 97) + shift) % 26);
                }
                hasil[i] = (char)alphabet[i]; //ubah inputan ke char
                Console.Write(hasil[i]);
            }
                //Console.WriteLine("Masukkan total karakter yang akan diinput : ");
                //int jumlahKarakter = int.Parse(Console.ReadLine());

                //Console.WriteLine("Masukkan Kalimat :");
                //String kalimat = Console.ReadLine();
                //Char[] kalimatArray = kalimat.ToCharArray();

                //if (kalimatArray.Length != jumlahKarakter)
                //{
                //    Console.WriteLine("Error ");
                //}
                //else 
                //{
                //    Console.WriteLine("Masukkan Alphabet yang akan dirotasi :");
                //    int tambahRotasi = int.Parse(Console.ReadLine());

                //    for (int i = 0; i < kalimatArray.Length; i++)
                //    {
                //        char kalimatCaesarCipher = kalimatArray[i];
                //        kalimatCaesarCipher = (char)(kalimatCaesarCipher + tambahRotasi);

                //        if (kalimatCaesarCipher > 'z')
                //        {
                //            kalimatCaesarCipher = (char)(kalimatCaesarCipher - 26);
                //        }
                //        else if (kalimatCaesarCipher < 'a')
                //        {
                //            kalimatCaesarCipher = (char)(kalimatCaesarCipher + 26);
                //        }

                //        kalimatArray[i] = kalimatCaesarCipher;
                //    }
                //    Console.WriteLine(kalimatArray);
                //}
            }
    }
}
