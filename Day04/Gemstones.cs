﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Gemstones
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukan jumlah kalimat yang di input:");
            string jumlah = Console.ReadLine();
            int length = Convert.ToInt16(jumlah);

            string[] hurufArray = new string[length];

            int lengthInput = 0;

            //input baris hurufnya
            while (lengthInput < length)
            {
                Console.WriteLine("Masukan baris huruf ke-" + (lengthInput + 1));
                string inputString = Console.ReadLine();

                hurufArray[lengthInput] = inputString.ToLower();

                lengthInput++;
            }

            char[] arrayAlphabet = new char[] {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

            int temp = 0;
            int count = 0;

            //proses 
            for (int i = 0; i < arrayAlphabet.Length; i++)
            {
                for (int j = 0; j < hurufArray.Length; j++)
                {
                    //jika array huruf mengandung alfabet maka nilai temp bertambah
                    if (hurufArray[j].Contains(arrayAlphabet[i]))
                    {
                        temp++;
                    }
                }

                if (temp == hurufArray.Length)
                {
                    count++;
                }
                temp = 0;
            }

            Console.WriteLine(count);

        }
    }
}
