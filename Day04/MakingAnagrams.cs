﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MakingAnagrams
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan kalimat pertama");
            string kalimat1 = Console.ReadLine();

            Console.WriteLine("Masukkan kalimat kedua");
            string kalimat2 = Console.ReadLine();

            char[] character1 = kalimat1.ToLower().ToCharArray();
            char[] character2 = kalimat2.ToLower().ToCharArray();

            //untuk menghitung banyak karakter dari kedua inputan
            int totalKarakter = kalimat1.Length + kalimat2.Length; 

            for (int i = 0; i < kalimat1.Length; i++)
            {
                for (int j = 0; j < kalimat2.Length; j++)
                {
                    //jika karakter nya sama maka dihilangkan dua duanya dari total karakter kedua kalimat
                    if (kalimat1[i] == kalimat2[j])
                    {
                        totalKarakter -= 2;
                    }
                }
            }
            Console.WriteLine(totalKarakter);

        }
    }
}
