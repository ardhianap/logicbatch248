﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class StrongPassword
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Enter the password");
            string password = Console.ReadLine();

            char[] passwordChar = password.ToCharArray();

            int lowerChar = 0;
            int upperChar = 0;
            int uniqueChar = 0;
            int numberChar = 0;
            int totalFlaw = 0;

            for (int i = 0; i < passwordChar.Length; i++)
            {
                if (char.IsLower(passwordChar[i]))
                {
                    lowerChar += 1;
                }
                if (char.IsUpper(passwordChar[i]))
                {
                    upperChar += 1;
                }
                if (!char.IsLetterOrDigit(passwordChar[i]))
                {
                    uniqueChar += 1;
                }
                if (char.IsNumber(passwordChar[i]))
                {
                    numberChar += 1;
                }
            }

            if (lowerChar < 1)
            {
                totalFlaw++;
            }
            if (upperChar < 1)
            {
                totalFlaw++;
            }
            if (uniqueChar < 1)
            {
                totalFlaw++;
            }
            if (numberChar < 1)
            {
                totalFlaw++;
            }

            if (passwordChar.Length < 6)
            {
                Console.WriteLine("Minimal password is 6 digit, try again");

                if (totalFlaw > (6 - passwordChar.Length))
                {
                    Console.WriteLine(totalFlaw);
                }
                else
                {
                    Console.WriteLine(6 - password.Length);
                }
            }
            else
            {
                Console.WriteLine(totalFlaw);
            }
            Console.WriteLine();

        }
    }
}
