﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Palindrome
    {
        public static void Resolve()
        {
            Console.WriteLine();
            bool isPalindrome = false;

            Console.WriteLine("Masukkan Kalimat :");
            String stringKalimat = Console.ReadLine();
            String kalimatPenentu = stringKalimat;

            char[] arrayToChar = stringKalimat.ToCharArray();
            Array.Reverse(arrayToChar);
            String kebalikan = new string(arrayToChar);

            if (kalimatPenentu == kebalikan)
            {
                isPalindrome = true;
            }
            else
            {
                isPalindrome = false;
            }

            if ( isPalindrome == true)
            {
                Console.WriteLine("Benar, kalimat yang anda masukkan Palindrome");
            }
            else
            {
                Console.WriteLine("Salah, kalimat yang anda masukkan bukan Palindrome");
            }
        }
    }
}
