﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CamelCase
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan Kalimat :");
            String kalimat = Console.ReadLine();
            Char[] kalimatArray = kalimat.ToCharArray();
            int totalKata = 1;

            if (char.IsLower(kalimatArray[0]) && !kalimat.Contains(" "))
            {
                for (int i = 0; i < kalimatArray.Length; i++)
                {
                    if (char.IsUpper(kalimatArray[i]))
                    {
                        totalKata++;
                    }
                }
                Console.WriteLine("Total Kata : " + totalKata);
            }
            else
            {
                Console.WriteLine("Format Kalimat Bukan CamelCase");
            }  
        }
    }
}
