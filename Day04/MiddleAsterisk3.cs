﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MiddleAsterisk3
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukkan Kalimat :");
            String kalimat = Console.ReadLine();
            String[] kalimatArray = kalimat.Split(' ');

            String result = "";

            for (int i = 0; i < kalimatArray.Length; i++)
            {

                char[] kalimatArrayChar = kalimatArray[i].ToCharArray();
                for (int j = 0; j < kalimatArrayChar.Length; j++)
                {
                    if (j == 0 || j == kalimatArrayChar.Length - 1)
                    {
                        result += kalimatArrayChar[j].ToString();
                    }
                    else
                    {
                        result += '*'.ToString();
                    }
                }
                result += " ";
            }
            Console.WriteLine(result);
        }
    }
}
