﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class TwoStrings
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan kalimat pertama:");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Masukkan kalimat kedua:");
            string kalimat2 = Console.ReadLine();

            char[] kalimatChar = kalimat.ToCharArray();
            char[] kalimat2Char = kalimat2.ToCharArray();

            Boolean isTwoString = false;

            for (int i = 0; i < kalimatChar.Length; i++)
            {
                for (int j = 0; j < kalimat2Char.Length; j++)
                {
                    //jika karakter di kalimat pertama ada yang sama dengan karakter di kalimat kedua
                    if (kalimatChar[i].Equals(kalimat2Char[j]))
                    {
                        isTwoString = true;
                        break;
                    }
                }
            }

            if (isTwoString)
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }

        }
    }
}
