﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Game
    {
        public static void Resolve()
        {
            Console.WriteLine();

            //input menu
            Console.WriteLine("Mau Main Dahulu atau Komputer Dulu : ");
            Console.WriteLine("1. Main Dahulu");
            Console.WriteLine("2. Komputer Dahulu");
            int pilihan = int.Parse(Console.ReadLine());

            if (pilihan == 1)
            {
                //input angka
                Console.WriteLine("Masukkan angka yang mau diinput dari angka 1 - 10 : ");
                int player = int.Parse(Console.ReadLine());

                //proses random komputer
                Random angkaRandom = new Random();
                int computer = angkaRandom.Next(0,9);

                if (player < computer)
                {
                    //hasil
                    Console.WriteLine("Angka Anda : " + player);
                    Console.WriteLine("Angka Computer : " + computer);
                    Console.WriteLine("Computer Menang");
                }
                else
                {
                    //hasil
                    Console.WriteLine("Angka Anda : " + player);
                    Console.WriteLine("Angka Computer : " + computer);
                    Console.WriteLine("Anda Menang");
                }
            }
            else if (pilihan == 2)
            {
                //proses random
                Random angkaRandom = new Random();
                int computer = angkaRandom.Next(0, 9);

                Console.WriteLine("Computer Sudah Memilih Angkanya, Silahkan Giliran Anda !!!");

                //input angka
                Console.WriteLine("Masukkan angka yang mau diinput dari angka 1 - 10 : ");
                int player = int.Parse(Console.ReadLine());

                if (player < computer)
                {
                    //hasil
                    Console.WriteLine("Angka Anda : " + player);
                    Console.WriteLine("Angka Computer : " + computer);
                    Console.WriteLine("Computer Menang");
                }
                else
                {
                    //hasil
                    Console.WriteLine("Angka Anda : " + player);
                    Console.WriteLine("Angka Computer : " + computer);
                    Console.WriteLine("Anda Menang");
                }
            }

        }
    }
}
