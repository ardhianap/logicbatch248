﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Bank
    {
        public static void Resolve()
        {
            string answer = "Y";
            //perulangan while menu utama
            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine();
                Console.WriteLine("ATM Bank JAGO");

                //password dihardcode 123456
                string password = "123456";

                Console.WriteLine("Masukkan Pin Anda : ");
                string inputPassword = Console.ReadLine();

                //jika password benar
                if (inputPassword == password)
                {
                    string menu = "menu";

                    Console.WriteLine("Masukkan Saldo Terlebih Dahulu ");
                    long saldo = long.Parse(Console.ReadLine());

                    Console.WriteLine("Selamat Datang di Menu ATM Bank JAGO");

                    Console.WriteLine("1. Tranfer Sesama Bank");
                    Console.WriteLine("2. Tranfer Antar Bank");

                    Console.WriteLine("Masukkan Menu yang akan Anda Pilih : ");
                    int pilihanMenu = int.Parse(Console.ReadLine());

                    long biaya = 7500;

                    //rekening dihardcode 1234567890
                    //String Rekening = "1234567891";

                    //jika menu transfer sesama rekening
                    if (pilihanMenu == 1)
                    {
                        Console.WriteLine("Menu Transfer Sesama Bank");

                        Console.WriteLine("Masukkan nomor rekening : ");
                        string rekeningTujuanSesama = Console.ReadLine();

                        //jika rekening tujuan benar 10 digit
                        if (rekeningTujuanSesama.Length == 10)
                        {
                            Console.WriteLine("Masukkan Jumlah Saldo yang Akan Ditransfer : ");
                            long transferSesama = long.Parse(Console.ReadLine());

                            saldo = saldo - transferSesama;

                            //jika saldo > jumlah transfer
                            if (saldo > transferSesama)
                            {
                                Console.WriteLine("Transaksi Berhasil !");
                                Console.WriteLine("Anda mentransfer ke rekening : " + rekeningTujuanSesama);
                                Console.WriteLine("Tranfer Senilai : " + transferSesama);
                                Console.WriteLine("Saldo anda Menjadi " + saldo);
                            }
                            //jika saldo kurang
                            else
                            {
                                Console.WriteLine("Transaksi Gagal, Saldo Kurang !");
                            }
                        }
                        //jika no rek bukan 10 digit
                        else
                        {
                            Console.WriteLine("Nomor Rekening Salah Input, Harus 10 digit");
                        }
                    }
                    //jika menu transfer antar
                    else if (pilihanMenu == 2)
                    {
                        Console.WriteLine("Menu Transfer Antar Bank");

                        Console.WriteLine("Masukkan kode Bank : ");
                        string kodeBank = Console.ReadLine();

                        if (kodeBank.Length == 3)
                        {
                            Console.WriteLine("Masukkan nomor rekening : ");
                            string rekeningTujuanAntar = Console.ReadLine();

                            //jika no rek 10 digit
                            if (rekeningTujuanAntar.Length == 10)
                            {
                                Console.WriteLine("Masukkan Jumlah Saldo yang Akan Ditransfer : ");
                                long transferAntar = long.Parse(Console.ReadLine());

                                saldo = saldo - transferAntar - biaya;

                                //jika saldo > jumlah transfer
                                if (saldo > transferAntar)
                                {
                                    Console.WriteLine("Transaksi Berhasil !");
                                    Console.WriteLine("Anda mentransfer ke rekening : " + kodeBank + rekeningTujuanAntar);
                                    Console.WriteLine("Tranfer Senilai : " + transferAntar);
                                    Console.WriteLine("Saldo anda Menjadi " + saldo);
                                }
                                //jika saldo kurang
                                else
                                {
                                    Console.WriteLine("Transaksi Gagal, Saldo Kurang !");
                                }
                            }
                            //jika no rek bukan 10 digit
                            else
                            {
                                Console.WriteLine("Nomor Rekening Salah Input, Harus 10 digit ");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Kode Bank Harus 3 Digit");
                        }      
                    }
                    //jika salah menu
                    else
                    {
                        Console.WriteLine("Salah Input Menu");
                    }
                }
                else
                {
                    Console.WriteLine("Password Anda Salah");
                }

                Console.WriteLine("Kembali Ke Awal ?");
                answer = Console.ReadLine();
            }
        }
    }
}
