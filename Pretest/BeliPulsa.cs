﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class BeliPulsa
    {
        public static void Resolve()
        {
            Console.WriteLine();

            //input pulsa
            Console.WriteLine("Masukkan Jumlah Pulsa yang akan dibeli : ");
            int pulsa = int.Parse(Console.ReadLine());

            int poin = 0;

            //proses pencairan poin
            if (pulsa < 10000)
            {
                poin = 0;
            }
            else if (pulsa >= 10000 && pulsa <= 30000)
            {
                poin = (pulsa - 10000) / 1000;
            }
            else if (pulsa > 30000)
            {   
                poin = 20 + (((pulsa - 30000)/1000) * 2);
            }

            //hasil
            Console.WriteLine("Pulsa yang dibeli : " +pulsa + " dan Poin yang didapat = " + poin);

        }
    }
}
