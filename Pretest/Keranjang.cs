﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Keranjang
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan Total Buah Keranjang 1 :");
            string totalKeranjang1 = Console.ReadLine().ToLower();

            Console.WriteLine("Masukkan Total Buah Keranjang 2 :");
            string totalKeranjang2 = Console.ReadLine().ToLower();

            Console.WriteLine("Masukkan Total Buah Keranjang 3 :");
            string totalKeranjang3 = Console.ReadLine().ToLower();

            int totalSekarang = 0;
            Random keranjang = new Random();
            int keranjangDibawa = keranjang.Next(1, 3);

            if (totalKeranjang1 == "kosong")
            {
                totalKeranjang1 = "0";
            }
            if (totalKeranjang2 == "kosong")
            {
                totalKeranjang2 = "0";
            }
            if (totalKeranjang3 == "kosong")
            {
                totalKeranjang3 = "0";
            }


            if (keranjangDibawa == 1)
            {
                totalSekarang = int.Parse(totalKeranjang2) + int.Parse(totalKeranjang3);
            }

            else if (keranjangDibawa == 2)
            {
                totalSekarang = int.Parse(totalKeranjang1) + int.Parse(totalKeranjang3);
            }

            else if (keranjangDibawa == 3)
            {
                totalSekarang = int.Parse(totalKeranjang1) + int.Parse(totalKeranjang2);
            }


            Console.WriteLine("Keranjang yang dipakai by random : " + keranjangDibawa);
            Console.WriteLine("Total buah didapur :" + totalSekarang);


        }
    }
}
