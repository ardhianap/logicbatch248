﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Bensin
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan tempat lokasi yang akan dilalui : ");
            string lokasiTujuan = Console.ReadLine().ToUpper();

            string[] splitLokasi = lokasiTujuan.Split('-');
            int jarakTempuh = 0;

            for (int i = 0; i < splitLokasi.Length; i++)
            {
                if (splitLokasi[i] == "TOKO")
                {
                    if (jarakTempuh > 0)
                    {
                        if (splitLokasi[i - 1] == "TEMPAT4")
                        {
                            jarakTempuh = jarakTempuh + 6500;

                        }
                        else if (splitLokasi[i - 1] == "TEMPAT3")
                        {
                            jarakTempuh = jarakTempuh + 4000;

                        }
                        else if (splitLokasi[i - 1] == "TEMPAT2")
                        {
                            jarakTempuh = jarakTempuh + 2500;

                        }
                        else if (splitLokasi[i - 1] == "TEMPAT1")
                        {
                            jarakTempuh = jarakTempuh + 2000;

                        }
                        else if (splitLokasi[i - 1] == "TOKO")
                        {
                            jarakTempuh = jarakTempuh + 0;

                        }
                    }
                }
                else if (splitLokasi[i] == "TEMPAT1")
                {

                    if (splitLokasi[i - 1] == "TEMPAT4")
                    {
                        jarakTempuh = jarakTempuh + 4500;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT3")
                    {
                        jarakTempuh = jarakTempuh + 2000;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT2")
                    {
                        jarakTempuh = jarakTempuh + 500;

                    }
                    else if (splitLokasi[i - 1] == "TOKO")
                    {
                        jarakTempuh = jarakTempuh + 2000;
                    }


                }
                else if (splitLokasi[i] == "TEMPAT2")
                {
                    if (splitLokasi[i - 1] == "TEMPAT4")
                    {
                        jarakTempuh = jarakTempuh + 4000;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT3")
                    {
                        jarakTempuh = jarakTempuh + 2000;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT1")
                    {
                        jarakTempuh = jarakTempuh + 500;
                    }
                    else if (splitLokasi[i - 1] == "TOKO")
                    {
                        jarakTempuh = jarakTempuh + 2500;
                    }

                }
                else if (splitLokasi[i] == "TEMPAT3")
                {
                    if (splitLokasi[i - 1] == "TEMPAT4")
                    {
                        jarakTempuh = jarakTempuh + 2500;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT2")
                    {
                        jarakTempuh = jarakTempuh + 1500;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT1")
                    {
                        jarakTempuh = jarakTempuh + 2000;
                    }
                    else if (splitLokasi[i - 1] == "TOKO")
                    {
                        jarakTempuh = jarakTempuh + 4000;
                    }
                }

                else if (splitLokasi[i] == "TEMPAT4")
                {
                    if (splitLokasi[i - 1] == "TEMPAT3")
                    {
                        jarakTempuh = jarakTempuh + 2500;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT2")
                    {
                        jarakTempuh = jarakTempuh + 4000;

                    }
                    else if (splitLokasi[i - 1] == "TEMPAT1")
                    {
                        jarakTempuh = jarakTempuh + 4500;

                    }
                    else if (splitLokasi[i - 1] == "TOKO")
                    {
                        jarakTempuh = jarakTempuh + 6500;
                    }
                }
            }
            double totalBensin = 0;
            double temporary = Convert.ToDouble(jarakTempuh) / 1000;
            
            while (temporary > 0)
            {
                if (temporary > 2.5)
                {
                    temporary = temporary - 2.5;
                    totalBensin++;
                }

                else if (temporary < 2.5)
                {
                    totalBensin += temporary * 0.4;
                    temporary = 0;
                }
            }
            Console.WriteLine("Total Bensin Selama Perjalanan adalah " + totalBensin + " Liter");



        }
    }
}
