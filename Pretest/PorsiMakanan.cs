﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class PorsiMakanan
    {
        public static void Resolve()
        {
            Console.WriteLine("Resto Makanan");

            Console.WriteLine("Total Laki-laki Dewasa :");
            double lakiLakiDewasa = double.Parse(Console.ReadLine());

            Console.WriteLine("Total Perempuan Dewasa :");
            double perempuanDewasa = double.Parse(Console.ReadLine());

            Console.WriteLine("Total Anak-anak :");
            double anakKecil = double.Parse(Console.ReadLine());

            Console.WriteLine("Total balita :");
            double balita = double.Parse(Console.ReadLine());

            Console.WriteLine("Ada tambahan? (Y/N)");
            string jawaban = (Console.ReadLine().ToUpper());

            while (jawaban.ToUpper() == "Y")
            {
                Console.WriteLine("1. Laki-laki Dewasa 2. Perempuan Dewasa 3.Anak 4.Balita");
                string tambahanPorsi = Console.ReadLine();
                Console.WriteLine();

                if ("1" == tambahanPorsi)
                {
                    Console.WriteLine("Berapa porsi lagi ? ");
                    double tambahPorsiMakanan = double.Parse(Console.ReadLine());
                    lakiLakiDewasa += tambahPorsiMakanan;
                }

                else if ("2" == tambahanPorsi)
                {
                    Console.WriteLine("Berapa porsi lagi ? ");
                    double tambahPorsiMakanan = double.Parse(Console.ReadLine());
                    perempuanDewasa += tambahPorsiMakanan;
                }

                else if ("3" == tambahanPorsi)
                {
                    Console.WriteLine("Berapa porsi lagi ? ");
                    double tambahPorsiMakanan = double.Parse(Console.ReadLine());
                    anakKecil += tambahPorsiMakanan;
                }

                else if ("4" == tambahanPorsi)
                {
                    Console.WriteLine("Berapa porsi lagi ? ");
                    double tambahPorsiMakanan = double.Parse(Console.ReadLine());
                    balita += tambahPorsiMakanan;
                }

                Console.WriteLine("Nambah Lagi? (Y/N)");
                jawaban = (Console.ReadLine().ToUpper());
                Console.WriteLine();
            }

            lakiLakiDewasa = lakiLakiDewasa * 2;
            perempuanDewasa = perempuanDewasa * 1;
            balita = balita * 1;
            anakKecil = anakKecil * 1 / 2;

            double totalPorsi = lakiLakiDewasa + perempuanDewasa + balita + anakKecil;

            Console.WriteLine(totalPorsi + " Porsi Makanan Keseluruhan");



        }
    }
}
