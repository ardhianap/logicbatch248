﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class EviternityNumber
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan angka:");
            int angkaInputan = int.Parse(Console.ReadLine());

            string temporary = "";
            int cek3 = 0;
            int cek5 = 0;
            int cek8 = 0;

            for (int i = 0; i < angkaInputan; i++)
            {
                if (i == 8)
                {
                    Console.Write(i + " ");
                }
                else if (i > 8)
                {
                    temporary = Convert.ToString(i);
                    if ((temporary.Contains('8') && temporary.Contains('5') && !temporary.Contains('1') && !temporary.Contains('2') && !temporary.Contains('4') && !temporary.Contains('6') && !temporary.Contains('7') && !temporary.Contains('9') && !temporary.Contains('0')))
                    {
                        char[] angka = temporary.ToCharArray();
                        for (int j = 0; j < angka.Length; j++)
                        {
                            if (angka[j] == '3')
                            {
                                cek3++;
                            }
                            if (angka[j] == '5')
                            {
                                cek5++;
                            }
                            if (angka[j] == '8')
                            {
                                cek8++;
                            }
                        }
                        if (cek5 <= cek8 && cek3 <= cek5)
                        {
                            Console.Write(i + " ");
                            cek3 = 0;
                            cek5 = 0;
                            cek8 = 0;
                        }
                        else
                        {
                            cek3 = 0;
                            cek5 = 0;
                            cek8 = 0;
                        }

                    }
                    else if (temporary.Contains('8') && !temporary.Contains('1') && !temporary.Contains('2') && !temporary.Contains('3') && !temporary.Contains('4') && !temporary.Contains('6') && !temporary.Contains('7') && !temporary.Contains('9') && !temporary.Contains('0'))
                    {
                        Console.Write(i + " ");
                    }
                }
            }
        }

    }
}
