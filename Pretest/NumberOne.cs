﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class NumberOne
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukan panjang n :");
            int panjang = int.Parse(Console.ReadLine());

            int bilanganDeret = 100;
            string angkaDeret = "";

            while (0 < panjang)
            {
                double temporary = 0;
                double hasilKuadrat = 0;
                angkaDeret = Convert.ToString(bilanganDeret);

                char[] deretAngka = angkaDeret.ToCharArray();
                hasilKuadrat = 0;

                for (int j = 0; j < deretAngka.Length; j++)
                {
                    temporary = Math.Pow((int)char.GetNumericValue(deretAngka[j]), 2);
                    hasilKuadrat += temporary;
                }

                if (hasilKuadrat >= 0 && hasilKuadrat <= 9)
                {
                    if (hasilKuadrat == 1)
                    {
                        Console.Write(bilanganDeret + " ");
                        panjang--;
                    }

                }
                else
                {
                    while (hasilKuadrat >= 10)
                    {
                        string tempHasilKuadrat = hasilKuadrat.ToString();
                        char[] tempChar = tempHasilKuadrat.ToCharArray();
                        temporary = 0;
                        hasilKuadrat = 0;
                        for (int k = 0; k < tempChar.Length; k++)
                        {
                            temporary = Math.Pow((int)char.GetNumericValue(tempChar[k]), 2);
                            hasilKuadrat += temporary;
                        }

                        if (hasilKuadrat >= 0 && hasilKuadrat <= 9)
                        {
                            if (hasilKuadrat == 1)
                            {
                                Console.Write(bilanganDeret + " ");
                                panjang--;
                            }

                        }
                    }
                }

                bilanganDeret++;

            }
            Console.WriteLine();

        }
    }
}
