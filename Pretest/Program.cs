﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";
            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soalnyo` :");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Beli Pulsa");
                        BeliPulsa.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("02. Bensin");
                        Bensin.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("03. Konversi Volume");
                        KonversiVolume.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("04. Saldo OPO");
                        SaldoOPO.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("05. Porsi Makanan");
                        PorsiMakanan.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("06. Game");
                        Game.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("07. Keranjang");
                        Keranjang.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("08. Bank");
                        Bank.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("09. Kartu");
                        Kartu.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("10. Deret Prima Fibonaci");
                        DeretPrimaFibonaci.Resolve();
                        break;
                    case 11:
                        Console.WriteLine("11. Number One");
                        NumberOne.Resolve();
                        break;
                    case 12:
                        Console.WriteLine("12. Eviternity Number");
                        EviternityNumber.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();

            }
        }
    }
}
