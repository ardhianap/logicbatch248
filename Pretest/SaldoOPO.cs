﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class SaldoOPO
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan Saldo OPO:");
            int saldoOpo = int.Parse(Console.ReadLine());

            double diskon = 0.5;
            double totalBelanja = 0;
            double cashback = 0;
            double cashbackpersen = 0.1;
            int totalKopi = 0;
            double temporary = saldoOpo;
            
            

            if (saldoOpo < 40000)
            {
                while (temporary < 40000 && temporary >= 18000)
                {
                    if (temporary >= 18000)
                    {
                        temporary -= 18000;
                        totalKopi++;
                    }
                }

                cashback = saldoOpo - temporary;
                cashback = cashback * cashbackpersen;
                temporary += cashback;
                Console.WriteLine("Jumlah Total Kopi = " + totalKopi);
                Console.WriteLine("Saldo Akhir OPO = " + temporary);

            }
            else
            {
                while (temporary > (18000 * diskon))
                {
                    temporary -= (18000 * diskon);
                    totalKopi++;
                }

                totalBelanja = saldoOpo - temporary;
                if (totalBelanja > 100000)
                {
                    Console.WriteLine("Tidak jadi");
                }

                else
                {
                    cashback = totalBelanja * cashbackpersen;
                    if (cashback > 30000)
                    {
                        cashback = 30000;
                    }
                    temporary += cashback;
                    Console.WriteLine("Jumlah Total Kopi = " + totalKopi);
                    Console.WriteLine("Saldo Akhir OPO = " + temporary);
                }

            }

        }
    }
}
