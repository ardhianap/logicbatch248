﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Kartu
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan Jumlah Kartu :");
            int kartu = int.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Masukan Penawaran Anda:");
            int penawaran = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine("Apakah Anda Sudah Siap Untuk Memainkannya? (Y/N):");
            string jawaban = Console.ReadLine().ToUpper();

            while (jawaban.ToUpper() == "Y")
            {
                Console.WriteLine();
                Console.WriteLine("Silahkan pilih Kotak A atau B");
                Console.WriteLine("Silahkan Input A atau B");

                Random komputer = new Random();
                int kotakA = komputer.Next(0, 9);
                int kotakB = komputer.Next(0, 9);
                string kotakPermainan = Console.ReadLine().ToUpper();

                if (kotakPermainan != "A" && kotakPermainan != "B")
                {
                    Console.WriteLine("Input Hanya A Atau B Saja");
                }

                else
                {

                    if (kotakPermainan == "A")
                    {
                        if (kotakB > kotakA)
                        {
                            kartu -= penawaran;
                        }
                        else if (kotakB < kotakA)
                        {
                            kartu += penawaran;
                        }
                    }

                    else if (kotakPermainan == "B")
                    {
                        if (kotakA > kotakB)
                        {
                            kartu -= penawaran;
                        }
                        else if (kotakA < kotakB)
                        {
                            kartu += penawaran;
                        }
                    }

                    if (kartu <= 0)
                    {
                        kartu = 0;
                        Console.WriteLine();
                        Console.WriteLine("Selamat, Anda Menang");

                        Console.WriteLine();
                        Console.WriteLine("Kotak A :" + kotakA + " Kotak B :" + kotakB);
                        Console.WriteLine("Total Kartu anda : " + kartu);
                        break;

                    }
                    else
                    {
                        Console.WriteLine("Kotak A :" + kotakA + " Kotak B :" + kotakB);
                        Console.WriteLine("Total Kartu anda : " + kartu);


                        Console.WriteLine();
                        Console.WriteLine("Lanjutkan Permainan? (Y/N):");
                        jawaban = Console.ReadLine().ToUpper();
                    }
                    if (jawaban.ToUpper() == "N")
                    {
                        Console.WriteLine("Anda Kalah Karena Menyerah Duluan");
                    }

                }

            }

        }
    }
}
