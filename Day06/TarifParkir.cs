﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class TarifParkir
    {
        public static void Resolve()
        {
            Console.WriteLine();

            //validasi kepenulisan waktu
            try
            {
                Console.WriteLine("Masukkan Waktu Masuk : ");
                string stringMasuk = Console.ReadLine();

                Console.WriteLine("Masukkan Waktu Keluar : ");
                string stringKeluar = Console.ReadLine();

                //konversi datetime               
                DateTime waktuMasuk = Convert.ToDateTime(stringMasuk);
                DateTime waktuKeluar = Convert.ToDateTime(stringKeluar);

                double selisihHari = (waktuKeluar - waktuMasuk).TotalDays;

                double lamaParkirDecimal = selisihHari * (double)24;
                int lamaParkir = (int)lamaParkirDecimal + 1;

                double biaya = 0;

                //validasi minus
                if (lamaParkir <= 0)
                {
                    Console.WriteLine("input yang anda masukkan salah!");
                }

                //8 jam pertama
                else if (lamaParkir < 8 && lamaParkir > 0)
                {
                    biaya = lamaParkir * 1000;
                    Console.WriteLine(biaya);
                }

                //lebih dari 8 jam dan kurang dari 24 jam
                else if (lamaParkir >= 8 && lamaParkir < 24)
                {
                    biaya = 8000;
                    Console.WriteLine(biaya);
                }

                //lebih dari 24 jam
                else
                {
                    //jika lebih dari 8 jam di lebih 24 jam
                    if (lamaParkir % 24 > 8)
                    {
                        biaya = ((lamaParkir / 24) * 23000);
                        Console.WriteLine(biaya);
                    }
                    //jika kurang dari 24 jam di lebih 24 jam
                    else
                    {
                        biaya = ((lamaParkir / 24) * 15000) + ((lamaParkir % 24) * 1000);
                        Console.WriteLine(biaya);
                    }
                }
                Console.WriteLine();
            }

            catch (Exception e)
            {
                //memunculkan error sistem pada console
                Console.WriteLine("Format waktu salah!");
            }
        }
    }
}


