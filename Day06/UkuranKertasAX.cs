﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class UkuranKertasAX
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan nomor kertas AX :");
            int nomorKertas = int.Parse(Console.ReadLine());

            double ukuranKertasA6 = 10.50 * 14.80;
            double ukuranKertasA5 = 14.80 * 21.00;
            double ukuranKertasA4 = 21.00 * 29.70;
            double ukuranKertasA3 = 29.70 * 42.00;
            double ukuranKertasA2 = 42.00 * 59.40;
            double ukuranKertasA1 = 59.40 * 84.10;
            double ukuranKertasA0 = 84.10 * 118.90;
            double ukuran = 0;

            if (nomorKertas == 0)
            {
                ukuran = ukuranKertasA0 / ukuranKertasA6;
            }
            else if (nomorKertas == 1)
            {
                ukuran = ukuranKertasA1 / ukuranKertasA6;
            }
            else if (nomorKertas == 2)
            {
                ukuran = ukuranKertasA2 / ukuranKertasA6;
            }
            else if (nomorKertas == 3)
            {
                ukuran = ukuranKertasA3 / ukuranKertasA6;
            }
            else if (nomorKertas == 4)
            {
                ukuran = ukuranKertasA4 / ukuranKertasA6;
            }
            else if (nomorKertas == 5)
            {
                ukuran = ukuranKertasA5 / ukuranKertasA6;
            }
            else if (nomorKertas == 6)
            {
                ukuran = ukuranKertasA6 / ukuranKertasA6;
            }
            else
            {
                Console.WriteLine("Nomor Kertas Tidak Ditemukan");
            }

            int ukuranHasil = Convert.ToInt32(ukuran);

            Console.WriteLine("Ukurannya kertas A"+nomorKertas+ " adalah " + ukuranHasil + " X Ukuran Kertas A6");

        }
    }
}
