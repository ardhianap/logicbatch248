﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {

            string answer = "Y";
            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal :");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Ukuran Kertas AX");
                        UkuranKertasAX.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("02. Urutan Abjad");
                        UrutanAbjad.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("03. Tarif Parkir");
                        TarifParkir.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("04. Belanja Online");
                        BelanjaOnline.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("05. Gambreng");
                        Gambreng.Resolve();
                        break;
                 
                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();

            }

        }
    }
}
