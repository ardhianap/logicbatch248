﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class BelanjaOnline
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("tanggal dan hari pemesanan");
            string pesan = Console.ReadLine().ToLower();
            string[] SplitPesan = pesan.Split(' ');

            int tanggalPesan = int.Parse(SplitPesan[0]);
            string hariPesan = SplitPesan[1];

            Console.WriteLine("Hari libur Nasional");
            string libur = Console.ReadLine();

            string[] stringliburArray = libur.Split(' ');
            int[] liburArray = new int[stringliburArray.Length];

            string[] hariArray = new string[] { "senin", "selasa", "rabu", "kamis", "jumat", "sabtu", "minggu" };

            for (int i = 0; i < liburArray.Length; i++)
            {
                liburArray[i] = Convert.ToInt32(stringliburArray[i]);
            }

            int indexHari = 0;
            for (int i = 0; i < hariArray.Length; i++)
            {
                if (hariArray[i] == hariPesan)
                {
                    indexHari = i;
                }
            }

            int lamaPengiriman = 7;
            int index = 0;
            string bulan = "";
            int temp = 0;
            while (lamaPengiriman > 0)
            {
                if (indexHari > 6)
                {
                    indexHari = 0;
                }
                if (liburArray[index] == tanggalPesan || index > 4)
                {
                    if (liburArray[index] == tanggalPesan)
                    {
                        if (index < liburArray.Length - 1)
                        {
                            index++;
                        }
                    }

                    if (tanggalPesan == 31)
                    {
                        tanggalPesan = 0;
                        bulan = " Bulan berikutnya";
                    }
                }
                else if (tanggalPesan != liburArray[index] && indexHari < 5)
                {
                    if (tanggalPesan == 31)
                    {
                        tanggalPesan = 0;
                        bulan = " Bulan berikutnya";
                    }
                    temp = tanggalPesan;
                    lamaPengiriman--;
                }
                indexHari++;
                tanggalPesan++;
            }
            Console.WriteLine("Pesanan sampai pada tanggal " + temp + bulan);
        }
    }
}
