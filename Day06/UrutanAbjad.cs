﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class UrutanAbjad
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukan Kalimat: ");
            string alfabet = Console.ReadLine().ToLower();

            char[] alfabetChar = alfabet.ToCharArray();

            string pengecekan = "abcdefghijklmnopqrstuvwxyz ";
            char[] pengecekanArray = pengecekan.ToCharArray();

            string hurufVokal = "";
            string hurufkonsonan = "";

            for (int i = 0; i < pengecekanArray.Length; i++)
            {
                for (int j = 0; j < alfabetChar.Length; j++)
                {
                    if (pengecekanArray[i] == alfabetChar[j])
                    {
                        if (pengecekanArray[i] == 'a' || pengecekanArray[i] == 'i' || pengecekanArray[i] == 'u' || pengecekanArray[i] == 'e' || pengecekanArray[i] == 'o')
                        {
                            hurufVokal = hurufVokal + pengecekanArray[i];
                        }
                        else
                        {
                            hurufkonsonan = hurufkonsonan + pengecekanArray[i];

                        }
                    }
                }
            }

            Console.WriteLine("Huruf Vokal : " + hurufVokal);
            Console.WriteLine("Huruf Konsonan : " + hurufkonsonan);

        }
    }
}
