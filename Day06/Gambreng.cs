﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Gambreng
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan gambreng A:");
            string gambrengA = Console.ReadLine();

            Console.WriteLine("Masukkan gambreng B:");
            string gambrengB = Console.ReadLine();

            char[] gambrengAChar = gambrengA.ToCharArray();
            char[] gambrengBChar = gambrengB.ToCharArray();

            int totalAMenang = 0;
            int totalAKalah = 0;
            int totalASeri = 0;
            int totalBSeri = 0;
            int totalBMenang = 0;
            int totalBKalah = 0;

            if (gambrengAChar.Length != gambrengBChar.Length)
            {
                Console.WriteLine("Inputan Salah Panjangnya");
            }
            else
            {
                for (int i = 0; i < gambrengAChar.Length; i++)
                {
                    //jika A Batu dan B Kertas
                    if (gambrengAChar[i] == 'B' && gambrengBChar[i] == 'K')
                    {
                        totalAKalah += 1;
                        totalBMenang += 1;
                    }
                    else if (gambrengAChar[i] == 'B' && gambrengBChar[i] == 'B')
                    {
                        totalASeri += 1;
                        totalBSeri += 1;
                    }
                    else if (gambrengAChar[i] == 'B' && gambrengBChar[i] == 'G')
                    {
                        totalAMenang += 1;
                        totalBKalah += 1;
                    }
                    else if (gambrengAChar[i] == 'K' && gambrengBChar[i] == 'K')
                    {
                        totalASeri += 1;
                        totalBSeri += 1;
                    }
                    else if (gambrengAChar[i] == 'K' && gambrengBChar[i] == 'B')
                    {
                        totalAMenang += 1;
                        totalBKalah += 1;
                    }
                    else if (gambrengAChar[i] == 'K' && gambrengBChar[i] == 'G')
                    {
                        totalAKalah += 1;
                        totalBMenang += 1;
                    }
                    else if (gambrengAChar[i] == 'G' && gambrengBChar[i] == 'K')
                    {
                        totalAMenang += 1;
                        totalBKalah += 1;
                    }
                    else if (gambrengAChar[i] == 'G' && gambrengBChar[i] == 'B')
                    {
                        totalAKalah += 1;
                        totalBMenang += 1;
                    }
                    else if (gambrengAChar[i] == 'G' && gambrengBChar[i] == 'G')
                    {
                        totalASeri += 1;
                        totalBSeri += 1;
                    }
                }

                Console.WriteLine("Perolehan A adalah Menang = " + totalAMenang + " Seri = " + totalASeri + " Kalah = " + totalAKalah);
                Console.WriteLine("Perolehan B adalah Menang = " + totalBMenang + " Seri = " + totalBSeri + " Kalah = " + totalBKalah);

                if (totalAMenang > totalBMenang)
                {
                    Console.WriteLine("Hasilnya A Menang ");
                }
                else if (totalAMenang < totalBMenang)
                {
                    Console.WriteLine("Hasilnya B Menang ");
                }
                else
                {
                    Console.WriteLine("Hasilnya Seri");
                }
            }  
        }
    }
}
