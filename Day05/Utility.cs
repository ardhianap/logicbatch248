﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Utility
    {
        public static int[] ConvertStringToIntArray(String numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numbersArray;
        }

        public static int[] ConvertStringToIntArray1(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }


            return numbersArray;
        }

        public static int[] ConvertStringToIntArray2(string numbers2)
        {
            string[] stringNumbersArray2 = numbers2.Split(' ');
            int[] numbersArray2 = new int[stringNumbersArray2.Length];

            //Convert to int
            for (int i = 0; i < numbersArray2.Length; i++)
            {
                numbersArray2[i] = int.Parse(stringNumbersArray2[i]);
            }


            return numbersArray2;
        }
    }
}
