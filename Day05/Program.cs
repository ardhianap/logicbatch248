﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Program
    {
        static void Main(string[] args)
        {

            string answer = "Y";
            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal :");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Bilangan Prima");
                        BilanganPrima.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("02. Es Loli");
                        EsLoli.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("03. Modus");
                        Modus.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("04. Custom Sorting");
                        CustomSort.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("05. Pustaka");
                        Pustaka.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("06. Integer Kacamata Baju");
                        IntKacamataBaju.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("07. Lilin Fibonaci");
                        LilinFibonaci.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("08. Integer Geser");
                        IntegerGeser.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("09. Parkir");
                        Parkir.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("10. Naik Gunung");
                        NaikGunung.Resolve();
                        break;
                    case 11:
                        Console.WriteLine("11. Kaos Kaki");
                        KaosKaki.Resolve();
                        break;
                    case 12:
                        Console.WriteLine("12. Pembulatan Akhir");
                        PembulatanAkhir.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();

            }

        }
    }
}
