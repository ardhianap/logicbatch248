﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Pustaka
    {
        public static void Resolve()
        {
            Console.WriteLine("Berapa Buku yang dipinjam : ");
            int buku = int.Parse(Console.ReadLine());

            Console.WriteLine("Input Tanggal Peminjaman : ");
            string waktuPeminjamanString = Console.ReadLine();

            Console.WriteLine("Input Tanggal Pengembalian : ");
            string waktuPengembalianString = Console.ReadLine();

            DateTime waktuPeminjaman = Convert.ToDateTime(waktuPeminjamanString);
            DateTime waktuPengembalian = Convert.ToDateTime(waktuPengembalianString);

            int lamaPeminjaman = (waktuPengembalian - waktuPeminjaman).Days;
            int jumlahDenda = ((buku * lamaPeminjaman) * 5000) - (15000 * buku);

            if (lamaPeminjaman > 3)
            {
                Console.WriteLine(jumlahDenda);
            }
            else if (lamaPeminjaman < 0)
            {
                Console.WriteLine("Salah Input");
            }
            else
            {
                Console.WriteLine("Tidak ada denda");
            }
        }
    }
}
